#!/usr/bin/python
from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

class Notification(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  title = db.Column(db.String(100))
  date = db.Column(db.String(11))
  time = db.Column(db.String(9))
  venue = db.Column(db.String(20))

@app.route('/notifications', methods = ['GET'])
def notifications():

    notification_list = Notification.query.all()

    notifications = []

    for notification in notification_list:
        notifications.append({"id": notification.id, "title": notification.title, "date": notification.date, "time": notification.time, "venue": notification.venue})

    return jsonify({'notifications': notifications})

@app.route('/add_notification', methods = ['POST'])
def add_notification():
    notication_data = request.get_json()

    new_notication = Notification(title = notication_data['title'], date = notication_data['date'], time = notication_data['time'], venue = notication_data['venue'])

    db.session.add(new_notication)
    db.session.commit()

    return 'Done', 201

@app.route('/delete_notification/<id_>')
def delete_notification(id_):
    notification_to_delete=db.session.query(Notification).filter_by(id=id_).one()
    db.session.delete(notification_to_delete)
    db.session.commit()
    return 'Notification Removed'

if __name__ == '__main__':
    app.run(debug=True)
